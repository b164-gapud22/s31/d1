// Setup dependencies
const express = require('express');
const mongoose = require('mongoose');
// This allows us to use ALL the routes defined in "taskRoute.js"
const taskRoute = require('./routes/taskRoute')
;

// Server Setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://magicgapud:magicgapud@magic.hb85z.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Routes
app.use("/tasks", taskRoute);
// http://localhost:3001/tasks/







app.listen(port, () => console.log(`Now listening to port ${port}`));



/*
Notes:
models > controllers > routes > index.js
*/

