const Task = require('../models/task');

// Controller Function for getting all the tasks

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};



// Creating a task
module.exports.createTask = (requestBody) => {

	// Create object
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else{
			return task;
		}
	})
}



// Deleting a task
// "id" url parameter pass from the taskRoute.js

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{
		if(err){
			console.log(err);
			return false;
		} else{
			return removedTask;
		}
	})
}

// Update a task

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}else{
				return updateTask;
			}
		})
	})
}

// ACTIVITY

// Specific task
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then((res, err) => {
		if(err){
			console.log(err);
			return false;
		}else{
			return res;
		}
	})
}


// Changing the status of a task
module.exports.changeTask = (taskId, changeStatus) => {
	return Task.findById(taskId).then((res, err) =>{
		if(err){
			console.log(err);
			return false;
		}

		res.status = "complete";

		return res.save().then((changeStatus, err) => {
			if(err){
				console.log(err);
				return false;
			}else{
				return changeStatus;
			}
		})

	})
}